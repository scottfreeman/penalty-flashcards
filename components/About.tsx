import React from 'react';
import { Modal, ModalBody, ModalButtons } from './Modal';
import Toggle from './Toggle';

const Settings = ({ active, modal, setModal }) => {
  return (
    <Modal innerRef={modal} active={active} title="About" setModal={setModal}>
      <ModalBody>
        <div className="mb-4 text-2xl">Penalty Flashcards</div>
        <p className="mb-2">
          This is a resource for Roller Derby officials, both skating and
          non-skating, to practice calling and recognising penalties.
        </p>
        <p className="mb-2">
          You can use the app to generate a random skater number, colour, and
          penalty. With that information you can then practice calling the
          penalty.
        </p>
        <p className="mb-2">
          The app will tell you the full verbal cue, the NSO code, and an image
          of the hand signal. This is great to learn, to test yourself, or
          challenge your friends.
        </p>
        <p>
          Built by{' '}
          <a
            href="https://gitlab.com/scottfreeman/penalty-flashcards"
            className="underline text-blue-800"
          >
            Scott Freeman
          </a>
          , free for all to use.
        </p>
      </ModalBody>
      <ModalButtons>
        <button
          onClick={() => {
            setModal(false);
          }}
          className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-green-600 uppercase tracking-wider text-white hover:bg-green-700 sm:ml-3 sm:w-auto sm:text-sm"
        >
          Close
        </button>
      </ModalButtons>
    </Modal>
  );
};

export default Settings;
