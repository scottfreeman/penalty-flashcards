import React from 'react';

const Button = ({ update }) => {
  return (
    <div className="w-full md:container bg-gray-900 text-white text-center pt-4">
      <button
        className="border border-transparent rounded bg-green-600 hover:bg-green-700 px-8 py-3 uppercase tracking-wider"
        onClick={update}
      >
        Refresh
      </button>
    </div>
  );
};

export default Button;
