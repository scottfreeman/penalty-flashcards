import React from 'react';

export const ModalBody = ({ children }) => {
  return <>{children}</>;
};

export const ModalButtons = ({ children }) => {
  return (
    <>
      <div className="px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
        {children}
      </div>
    </>
  );
};

const TailwindClasses = () => {
  return (
    <>
      <div className="opacity-75"></div>
      <div className="opacity-100"></div>
    </>
  );
};

export const Modal = ({ active, innerRef, setModal, title, ...props }) => {
  const arrayChildren = React.Children.toArray(props.children);
  let modalBody: React.ReactFragment;
  let modalButtons: React.ReactFragment;

  arrayChildren.map((child: React.ReactElement) => {
    modalBody = child.type === ModalBody ? child : modalBody;
    modalButtons = child.type === ModalButtons ? child : modalButtons;
  });

  if (!modalBody) modalBody = props.children;

  return (
    <>
      {active ? (
        <div
          className="fixed z-10 inset-0 overflow-y-auto"
          aria-labelledby="modal-title"
          role="dialog"
          aria-modal="true"
          ref={innerRef}
        >
          <div className="block items-end justify-center min-h-screen text-center p-0">
            <div
              className="js-overlay fixed inset-0 bg-gray-900 transition-opacity opacity-0 duration-500"
              aria-hidden="true"
            ></div>
            <span
              className="inline-block align-middle h-screen"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <div className="js-modal duration-500 inline-block bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all my-8 align-middle max-w-lg w-11/12 opacity-0 -translate-y-10">
              <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div className="sm:flex sm:items-start">
                  <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                    <div className="mt-2">{modalBody}</div>
                  </div>
                </div>
              </div>
              {modalButtons}
            </div>
          </div>
        </div>
      ) : null}
      <TailwindClasses />
    </>
  );
};
