import React, { useState } from 'react';

const Toggle = ({ label, initialState, onToggle }) => {
  const [checked, setChecked] = useState(initialState);

  return (
    <div className="flex items-center">
      <label className="flex items-center cursor-pointer">
        <div className="relative">
          <input
            type="checkbox"
            className="sr-only"
            checked={checked}
            onChange={() => {
              setChecked(!checked);
              onToggle(!checked);
            }}
          />
          <div className="w-10 h-4 bg-gray-300 rounded-full shadow-inner"></div>
          <div className="dot absolute w-6 h-6 bg-gray-200 rounded-full shadow -left-1 -top-1 transition"></div>
        </div>
        <div className="ml-3">{label}</div>
      </label>
    </div>
  );
};

export default Toggle;
