import React from 'react';
import Image from 'next/image';

const PenaltyData = ({ skaterPenalty, settings }) => {
  return (
    <div className="w-full md:container bg-gray-900 text-white">
      <div
        className={`mb-1 p-4 text-center text-4xl font-bold border-4 border-l-0 border-r-0 border-white skater-${skaterPenalty.skater.color}`}
      >
        {skaterPenalty.skater.number}
      </div>
      <div className="p-4 text-center text-lg">
        <div className="text-xs text-gray-400 uppercase">Penalty</div>
        <div className="text-2xl">{skaterPenalty.penalty.name}</div>
      </div>
      {settings.verbalCue && (
        <div className="p-4 text-center text-lg capitalize">
          <div className="text-xs text-gray-400 uppercase">Verbal Cue</div>
          <div className="text-2xl">
            {skaterPenalty.skater.color}, {skaterPenalty.skater.words},{' '}
            {skaterPenalty.penalty.name}
          </div>
        </div>
      )}
      {settings.nsoCode && (
        <div className="p-4 text-center text-lg">
          <div className="text-xs text-gray-400 uppercase">NSO Code</div>
          <div className="text-2xl">{skaterPenalty.penalty.nso}</div>
        </div>
      )}
      {settings.handSignals && (
        <div className="p-4 text-center text-lg">
          <div className="text-xs text-gray-400 uppercase">Hand Signal</div>
          <div className="mt-2 bg-white border border-transparent rounded-lg p-4 max-w-sm m-auto">
            <div className="relative w-full h-52">
              <Image
                src={`/handsignals/${skaterPenalty.penalty.image}.jpg`}
                layout="fill"
                objectFit="contain"
                alt={skaterPenalty.penalty.name}
              />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default PenaltyData;
