import React from 'react';
import Script from 'next/script';

const Analytics = () => {
  const GAID = process.env.NEXT_PUBLIC_GAID;

  if (GAID) {
    return (
      <>
        <Script
          src="https://www.googletagmanager.com/gtag/js?id=G-WS8NM458YF"
          strategy="afterInteractive"
        />
        <Script id="google-analytics" strategy="afterInteractive">
          {`
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());        
            gtag('config', 'G-WS8NM458YF');
            `}
        </Script>
      </>
    );
  } else {
    return <></>;
  }
};

export default Analytics;
