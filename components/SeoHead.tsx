import React from 'react';
import Head from 'next/head';

const SeoHead = () => {
  return (
    <Head>
      <title>Penalty Flashcards</title>
      <meta
        name="description"
        content="Roller Derby Officials - Penalty Flashcards. A resource for practising calling and recognising penalties."
      />
      <meta
        name="keywords"
        content="roller derby, roller derby officials, penalty flashcards"
      />
      <meta name="author" content="Scott Freeman <email@scottfreeman.net>" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
    </Head>
  );
};

export default SeoHead;
