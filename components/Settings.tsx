import React from 'react';
import { Modal, ModalBody, ModalButtons } from '../components/Modal';
import Toggle from './Toggle';

const Settings = ({ active, modal, setModal, settings, setSettings }) => {
  const updateSettings = (option, flag) => {
    settings[option] = flag;
    setSettings(settings);
  };

  const settingsKeys = Object.keys(settings);

  const friendlySettingsNames = {
    autoRefresh: 'Auto Refresh',
    handSignals: 'Hand Signals',
    verbalCue: 'Verbal Cue',
    nsoCode: 'NSO Code'
  };

  return (
    <Modal
      innerRef={modal}
      active={active}
      title="Settings"
      setModal={setModal}
    >
      <ModalBody>
        <div className="mb-8 text-2xl">Settings</div>
        <div className="flex flex-col items-center">
          {settingsKeys.map((name) => {
            return (
              <div className="mb-6 w-44" key={name}>
                <Toggle
                  initialState={settings[name]}
                  label={friendlySettingsNames[name]}
                  onToggle={(flag) => updateSettings(name, flag)}
                />
              </div>
            );
          })}
        </div>
      </ModalBody>
      <ModalButtons>
        <button
          onClick={() => {
            setModal(false);
          }}
          className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-green-600 uppercase tracking-wider text-white hover:bg-green-700 sm:ml-3 sm:w-auto sm:text-sm"
        >
          Save and close
        </button>
      </ModalButtons>
    </Modal>
  );
};

export default Settings;
