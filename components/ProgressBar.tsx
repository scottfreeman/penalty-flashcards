import React from 'react';

const ProgressBar = ({ autorefreshTimer }) => {
  return (
    <div className="mt-4 w-64 bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
      <div
        className="bg-blue-600 h-2.5 rounded-full transition-all"
        style={{ width: `${autorefreshTimer}%` }}
      ></div>
    </div>
  );
};

export default ProgressBar;
