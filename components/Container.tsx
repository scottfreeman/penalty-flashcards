import React from 'react';

const Container = ({ children }) => {
  return (
    <div className="bg-gray-900 h-screen flex flex-col align-middle content-center items-center">
      {children}
    </div>
  );
};

export default Container;
