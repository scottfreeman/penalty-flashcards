import { Skater, Penalty } from '../types';
import randomize from './randomize';
import colors from './colors';
import penalties from './penalties';
import numberWords from './numberWords';

const getNumber = () => {
  const trimmer = randomize(1, 4);
  let number = randomize(0, 9999);
  number = parseInt(number.toString().substring(0, trimmer));
  return number;
};

const getNumberWords = (number: number) => {
  let words = number
    .toString()
    .split('')
    .map((number) => numberWords[number])
    .join(' ');
  return words;
};

const getPenalty = () => {
  const penaltyNumber = randomize(0, penalties.length - 1);
  return penalties[penaltyNumber] as Penalty;
};

const getColor = () => {
  const colorNumber = randomize(0, colors.length - 1);
  return colors[colorNumber];
};

const getSkater = () => {
  const skaterNumber = getNumber();
  const skaterWords = getNumberWords(skaterNumber);
  const skaterColor = getColor();
  return {
    number: skaterNumber,
    words: skaterWords,
    color: skaterColor
  } as Skater;
};

export const fetchPenalty = () => {
  const skater = getSkater();
  const penalty = getPenalty();
  return { skater, penalty };
};
