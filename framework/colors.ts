const colors = [
  'white',
  'black',
  'red',
  'pink',
  'purple',
  'green',
  'yellow',
  'orange',
  'blue'
];

export default colors;
