const penalties = [
  {
    name: 'Back Block',
    image: 'b',
    nso: 'B',
    penalty: 'true'
  },
  {
    name: 'High Block',
    image: 'a',
    nso: 'A',
    penalty: 'true'
  },
  {
    name: 'Low Block',
    image: 'l',
    nso: 'L',
    penalty: 'true'
  },
  {
    name: 'Head Block',
    image: 'h',
    nso: 'H',
    penalty: 'true'
  },
  {
    name: 'Forearm',
    image: 'f',
    nso: 'F',
    penalty: 'true'
  },
  {
    name: 'Leg Block',
    image: 'e',
    nso: 'E',
    penalty: 'true'
  },
  {
    name: 'Illegal Contact',
    image: 'c',
    nso: 'C',
    penalty: 'true'
  },
  {
    name: 'Illegal Assist',
    image: 'c',
    nso: 'C',
    penalty: 'true'
  },
  {
    name: 'Early Hit',
    image: 'c',
    nso: 'C',
    penalty: 'true'
  },
  {
    name: 'Late Hit',
    image: 'c',
    nso: 'C',
    penalty: 'true'
  },
  {
    name: 'Out of Play Block',
    image: 'c',
    nso: 'C',
    penalty: 'true'
  },
  {
    name: 'Direction',
    image: 'd',
    nso: 'D',
    penalty: 'true'
  },
  {
    name: 'Stop Block',
    image: 'd',
    nso: 'D',
    penalty: 'true'
  },
  {
    name: 'Multiplayer',
    image: 'm',
    nso: 'M',
    penalty: 'true'
  },
  {
    name: 'Illegal Position',
    image: 'p',
    nso: 'P',
    penalty: 'true'
  },
  {
    name: 'Destruction',
    image: 'p',
    nso: 'P',
    penalty: 'true'
  },
  {
    name: 'Skating Out of Bounds',
    image: 'p',
    nso: 'P',
    penalty: 'true'
  },
  {
    name: 'Failure to Reform',
    image: 'p',
    nso: 'P',
    penalty: 'true'
  },
  {
    name: 'Failure to Return',
    image: 'p',
    nso: 'P',
    penalty: 'true'
  },
  {
    name: 'Failure to Yield',
    image: 'p',
    nso: 'P',
    penalty: 'true'
  },
  {
    name: 'Cut',
    image: 'x',
    nso: 'X',
    penalty: 'true'
  },
  {
    name: 'Illegal Re-Entry',
    image: 'x',
    nso: 'X',
    penalty: 'true'
  },
  {
    name: 'Interference',
    image: 'n',
    nso: 'N',
    penalty: 'true'
  },
  {
    name: 'Delay of Game',
    image: 'n',
    nso: 'N',
    penalty: 'true'
  },
  {
    name: 'Illegal Procedure',
    image: 'i',
    nso: 'I',
    penalty: 'true'
  },
  {
    name: 'Star Pass Violation',
    image: 'i',
    nso: 'I',
    penalty: 'true'
  },
  {
    name: 'Pass Interference',
    image: 'i',
    nso: 'I',
    penalty: 'true'
  },
  {
    name: 'Misconduct',
    image: 'g',
    nso: 'G',
    penalty: 'true'
  },
  {
    name: 'Insubordination',
    image: 'g',
    nso: 'G',
    penalty: 'true'
  }
];

export default penalties;
