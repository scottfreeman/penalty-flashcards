export interface SkaterPenalty {
  skater: Skater;
  penalty: Penalty;
}

export interface Skater {
  color: string;
  number: number;
  words: string;
}

export interface Penalty {
  name: string;
  image: string;
  nso: string;
  penalty: string;
}

export interface UserSettings {
  autoRefresh: boolean;
  handSignals: boolean;
  verbalCue: boolean;
  nsoCode: boolean;
}
