import { useEffect, useRef, useState } from 'react';

import useModal from '../hooks/useModal';

import { fetchPenalty } from '../framework/penalty';

import SeoHead from '../components/SeoHead';
import Container from '../components/Container';
import Header from '../components/Header';
import PenaltyData from '../components/PenaltyData';
import Button from '../components/Button';
import Settings from '../components/Settings';
import ProgressBar from '../components/ProgressBar';
import { SkaterPenalty, UserSettings } from '../types';
import About from '../components/About';
import Analytics from '../components/Analytics';

const defaultUserSettings: UserSettings = {
  autoRefresh: false,
  handSignals: true,
  verbalCue: true,
  nsoCode: true
};

const defaultSkaterPenalty: SkaterPenalty = {
  skater: {
    number: 0,
    color: '',
    words: ''
  },
  penalty: {
    name: '',
    nso: '',
    image: '',
    penalty: ''
  }
};

export default function Home() {
  const [skaterPenalty, setSkaterPenalty] = useState(defaultSkaterPenalty);
  const [settings, setSettings] = useState(defaultUserSettings);
  const [autorefreshTimer, setAutorefreshTimer] = useState(0);
  const updatePenalty = () => setSkaterPenalty(fetchPenalty);

  const settingsModal = useRef();
  const aboutModal = useRef();
  const { setModal: setSettingsModal, active: settingsModalActive } =
    useModal(settingsModal);
  const { setModal: setAboutModal, active: aboutModalActive } =
    useModal(aboutModal);

  useEffect(() => {
    updatePenalty();
  }, []);

  let autoRefresh: NodeJS.Timeout;

  if (settings.autoRefresh) {
    autoRefresh = setTimeout(() => {
      let count = autorefreshTimer + 1;
      if (count < 100) {
        setAutorefreshTimer(count);
      } else {
        setAutorefreshTimer(0);
        updatePenalty();
      }
    }, 75);
  } else {
    clearTimeout(autoRefresh);
  }

  return (
    <>
      <Analytics />
      <SeoHead />
      <Container>
        <Header
          setSettingsModal={setSettingsModal}
          setAboutModal={setAboutModal}
        />
        <PenaltyData skaterPenalty={skaterPenalty} settings={settings} />
        {settings.autoRefresh && (
          <ProgressBar autorefreshTimer={autorefreshTimer} />
        )}
        {!settings.autoRefresh && <Button update={updatePenalty} />}
      </Container>

      <Settings
        modal={settingsModal}
        active={settingsModalActive}
        setModal={setSettingsModal}
        settings={settings}
        setSettings={setSettings}
      />

      <About
        modal={aboutModal}
        active={aboutModalActive}
        setModal={setAboutModal}
      />
    </>
  );
}
