import { useEffect, useState } from 'react';

const useModal = (modal, isActive = false) => {
  const [active, setActive] = useState(isActive);
  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    if (isActive) setIsVisible(true);
  }, []);

  useEffect(() => {
    const overlayEl = modal?.current?.querySelector('.js-overlay');
    const modalEl = modal?.current?.querySelector('.js-modal');

    if (overlayEl && modalEl) {
      if (isVisible) {
        overlayEl.classList.remove('opacity-0');
        overlayEl.classList.add('opacity-75');
        modalEl.classList.remove('opacity-0');
        modalEl.classList.add('opacity-100');
        modalEl.classList.remove('-translate-y-10');
      } else {
        overlayEl.classList.remove('opacity-75');
        overlayEl.classList.add('opacity-0');
        modalEl.classList.remove('opacity-100');
        modalEl.classList.add('opacity-0');
        modalEl.classList.add('-translate-y-10');
        overlayEl.addEventListener('transitionend', handleCloseTransition);
      }
    }

    return () => {
      if (overlayEl)
        overlayEl.removeEventListener('transitionend', handleCloseTransition);
    };
  }, [isVisible]);

  const handleCloseTransition = () => {
    setIsVisible(false);
    setActive(false);
  };

  const setModal = (isActive: boolean) => {
    if (!active) setActive(true);
    setIsVisible(isActive);
  };

  return {
    setModal,
    active
  };
};

export default useModal;
