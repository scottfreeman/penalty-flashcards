# Penalty Flashcards

## A roller derby official resource.

Shows a random colour, skater number, and penalty. You can then use this to practise calling penalties!

Options allow you to show or hide the full verbal cue, the NSO code, and the hand signal image.

You can also set it to automatically refresh for hands-free mode!

To view the compiled project in your browser: https://flashcards.scottfreeman.net/

Best viewed in mobile.

Build in NextJS / React / Tailwind.  
Images grabbed from WFTDA rulebooks.  
Feel free to submit issues, fork, or use the code here however you want.
